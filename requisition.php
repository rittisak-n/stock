<?php
    session_start();
    if(!isset($_SESSION['username'])){
      $_SESSION['msg'] = "You must login first";
      header("location: login.php");
    }      
    if(isset($_GET['logout'])){
      session_destroy();
      unset($_SESSION['username']);
      header("login.php");
    }
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Stock Control Systems</title>
  </head>
  <style>
    .result.active{ background-color: lightgreen !important;}
  </style>
  <body>
    <div class="ui container">
      <?php include('./view/header.php');?>
      <div class="title">
        <h1>Requisition System</h1>
        <div class="ui form">
          <div class="two fields">
            <div class="field">
              <label>Requisition Number(เลขที่ใบเบิก)</label>
              <input type="text" id="searchNumber" placeholder="Order Number" />
              <div class="ui error message"></div>
            </div>
            <div class="field">
              <label>Requester Name(ผู้ขอเบิก)</label>
              <input type="text" id="searchName" placeholder="Username" />
              <div class="ui error message"></div>
            </div>
          </div>

          <div class="two fields">
            <div class="field">
              <label>Branch (สาขา)</label>
              <select
                class="ui fluid dropdown"
                name="selectBranch"
                id="selectBranch"
              ></select>
            </div>

            <div class="field">
              <label>Status (สถานะใบเบิก)</label>
              <select class="ui fluid dropdown" name="ststus" id="status">
                <option value="0">ทุกสถานะ</option>
                <option value="1">รอดำเนินการ</option>
                <option value="2">ดำเนินการเรียบร้อย</option>
              </select>
            </div>
          </div>

          <div class="two fields">
            <div class="field">
              <label>Start date (วันที่เริ่มต้น)</label>
              <div class="ui calendar" id="rangestart">
                <div class="ui input left icon">
                  <i class="calendar icon"></i>
                  <input type="text" placeholder="Start" id="startDate" />
                </div>
              </div>
            </div>
            <div class="field">
              <label>End date (วันที่สิ้นสุด)</label>
              <div class="ui calendar" id="rangeend">
                <div class="ui input left icon">
                  <i class="calendar icon"></i>
                  <input type="text" placeholder="End" id="endDate" />
                </div>
              </div>
            </div>
          </div>
          <div class="ui grid">
            <div class="six wide column">
              <input
                type="button"
                class="ui button "
                value="Search"
                onclick="getRequisition(0)"
              />
              <input
                type="button"
                class="ui button primary"
                id="btnCreate"
                value="Create"
                data-bs-toggle="modal"
                data-bs-target="#exampleModal"
              />
              <button class="ui icon grey button" 
              id="btnPending"
                value="ใบเบิกยังไม่อนุมัติ"
                data-bs-toggle="modal"
                data-bs-target="#modalPending"
                onclick="getRequisition(1)">
  <i class="copy outline icon"></i>
  ใบเบิกยังไม่อนุมัติ
</button>
              <!-- <input
                type="button"
                class="ui button basic"
                id="btnPending"
                value="ใบเบิกยังไม่อนุมัติ"
                data-bs-toggle="modal"
                data-bs-target="#modalPending"
                onclick="getRequisition(1)"
              /> -->
            </div>
          </div>
        </div>
      </div>

      <table class="ui celled table ">
        <thead>
          <tr class="center aligned">
            <th>No.</th>
            <th>Requisition Number</th>
            <th>Branch Name</th>
            <th>ผู้ขอเบิก</th>
            <th>Status</th>
            <th>Create Date(วันที่ขอเบิก)</th>
            <th></th>
          </tr>
        </thead>
        <tbody id="tbody"></tbody>
      </table>
    </div>

    <!-- Modal Create -->
    <div
      class="modal fade"
      id="exampleModal"
      tabindex="-1"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div class="modal-body">
            <form class="ui form">
              <div class="field">
                <label>Req Name(ชื่อผู้ขอเบิก)</label>
                <input
                  type="text"
                  name="userReq"
                  id="userReq"
                  placeholder="User Request"
                />
                <div class="ui error message"></div>
              </div>
              <div class="field">
                <label>Select Branch (สาขาผู้ขอเบิก)</label>
                <select
                  class="ui fluid dropdown"
                  name="branch"
                  id="branch"
                ></select>
              </div>

              <div class="field">
                <div class="ui search">
                  <div class="ui fluid icon input">
                    <input
                      id="txtProductName"
                      class="prompt"
                      type="text"
                      placeholder="Search by Product Name ..."
                      onkeyup="search_product(this.value)"
                      onkeypress="handle(event,this.value,1)"
                    />
                    <i class="search icon"></i>
                  </div>
                </div>
              </div>
              <div class="field">
                <table class="ui celled table">
                  <thead class="table-light align-middle">
                    <th scope="col">ProductCode</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">จำนวนในสต๊อก</th>
                    <th scope="col">Unit(จำนวนขอเบิก)</th>
                    <th scope="col">หมายเหตุ ผู้ขอเบิก</th>
                    <th scope="col"></th>
                  </thead>
                  <tbody id="dynamic_field"></tbody>
                </table>
              </div>
              <div class="field">
                <label>หมายเหตุ</label>
                <input
                  type="text"
                  name="comment"
                  id="comment"
                  placeholder="Comment"
                />
                <div class="ui error message"></div>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button type="button" class="btn btn-primary" onclick="createReq()">
              Save changes
            </button>
          </div>
        </div>
      </div>
    </div>

    <!--Modal Details-->
    <div class="modal" id="modalDetail" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Requisition Detail(รายละเอียดการขอเบิกสินค้า)</h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div class="modal-body">

            <div class="ui raised segment">
              <div class="ui equal width grid">
                <div
                  id="headerLeft"
                  class="right left aligned ten wide column headerLeft"
                ></div>
                <div
                  id="headerRight"
                  class="left aligned six wide column headerRight"
                ></div>
              </div>
            </div>
            <table class="ui celled table" id="tableRequisition">
              <thead class="table-light align-middle">
                <th scope="col">ProductCode</th>
                <th scope="col">Product Name</th>
                <th scope="col">จำนวนในสต๊อก</th>
                <th scope="col">จำนวนขอเบิก</th>
                <th scope="col">จำนวนที่ให้</th>
                <th scope="col">หมายเหตุ ผู้ขอเบิก</th>
                <th scope="col">หมายเหตุ ผู้ให้เบิก</th>
              </thead>
              <tbody id="requisitionDetails"></tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>

    <!--Modal Edit-->
    <div class="modal" id="modalEdit" tabindex="-1">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Requisition Detail(รายละเอียดการขอเบิกสินค้า)</h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>

          <div class="modal-body">
            <div class="ui raised segment">
              <div class="ui equal width grid">
                <div
                  id="headerLeft"
                  class="right left aligned ten wide column headerLeft"
                ></div>
                <div
                  id="headerRight"
                  class="left aligned six wide column headerRight"
                ></div>
              </div>
            </div>
            <form class="ui form">
              <h3 class="ui header"></h3>
              <div class="field">
                <label>Give Name (ชื่อผู้ให้เบิก)</label>
                <input
                  type="text"
                  name="userGive"
                  id="userGive"
                  placeholder="User Give"
                />
                <div class="ui error message"></div>
              </div>

              <table class="ui celled table">
                <thead class="table-light align-middle">
                  <th scope="col">ProductCode</th>
                  <th scope="col">Product Name</th>
	            	<th scope="col">จำนวนในสต๊อก</th>
                	<th scope="col">จำนวนขอเบิก</th>
                	<th scope="col">จำนวนที่ให้</th>
                	<th scope="col">หมายเหตุ ผู้ขอเบิก</th>
                	<th scope="col">หมายเหตุ ผู้ให้เบิก</th>
                </thead>
                <tbody
                  id="editRequisitionDetails"
                  class="editRequisitionDetails"
                ></tbody>
              </table>
            </form>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-primary"
              onclick="updateRequisitionDetail()"
            >
              Update changes
            </button>
            <button
              type="button"
              class="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
    <!--Success Modal-->
    <div id="modalSuccess" class="modal fade">
      <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div
            class="modal-header"
            style="background-color: rgb(134, 235, 134)"
          >
            <h2 class="ui center aligned icon header">
              <i class="check circle outline icon" style="color: white"></i>
            </h2>
          </div>
          <div class="modal-body">
            <h2 class="ui center aligned header">Successfully</h2>
            <h4 class="ui center aligned header">
              You transaction successfully !!
            </h4>
            <h6 class="ui center aligned header">
              <button
                type="button"
                class="ui green circular ui icon button"
                data-bs-dismiss="modal"
              >
                OK
              </button>
            </h6>
          </div>
        </div>
      </div>
    </div>

     <!--Cancel Modal-->
     <div id="modalCancel" class="modal fade">
      <div class="modal-dialog modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div
            class="modal-header"
            style="background-color: red"
          >
            <h2 class="ui center aligned icon header">
              <i class="times circle outline icon" style="color: white"></i>
            </h2>
          </div>
          <div class="modal-body">
            <h2 class="ui center aligned header">Confirm Delete</h2>
            <h4 class="ui center aligned header">
            You are about to delete one order, this procedure is irreversible.
            <p>Do you want to proceed?</p>
            </h4>
            <h6 class="ui center aligned header">
              <button
                type="button"
                class="ui red circular ui icon button"
                data-bs-dismiss="modal"
                onclick="cancelRequisition()"
              >
                OK
              </button>
              <button
                type="button"
                class="ui white circular ui icon button"
                data-bs-dismiss="modal"
              >
                No
              </button>
            </h6>
          </div>
        </div>
      </div>
    </div>

     <!--Modal Wating Status-->
     <div class="modal" id="modalPending" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Requisition Waiting Approved(ใบเบิกรอดำเนินการ)</h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div class="modal-body">
          <h2 class="ui header">List Order(รายการ)</h2>
            <table class="ui celled table">
        <thead>
          <tr class="center aligned">
            <th>No.</th>
            <th class="">Requisition Number</th>
            <th class="collapsing center aligned">Branch Name</th>
            <th class="collapsing center aligned">ชื่อผู้ขอเบิก</th>
            <th class="collapsing center aligned">สถานะ</th>
            <th class="collapsing center aligned">วันที่ขอเบิก</th>
          </tr>
        </thead>
        <tbody id="tbodyWaitApprove"></tbody>
      </table>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>



    <!--Google Icon-->
    <link
      href="https://fonts.googleapis.com/css2?family=Material+Icons"
      rel="stylesheet"
    />
    <!--Bootstrap v5-->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
      crossorigin="anonymous"
    ></script>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <!--Jquery-->
    <script
      src="https://code.jquery.com/jquery-3.6.0.js"
      integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
      crossorigin="anonymous"
    ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <!--Semitic UI-->
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      crossorigin="anonymous"
    ></script>
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>

    <!--Date Picker-->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <link
      rel="stylesheet"
      type="text/css"
      href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.8/dist/semantic.min.js"></script>

    <!--Print Preview-->

    <!--java script-->
    <script src="./js/requisition.js"></script>
  </body>
</html>
<script>
  

  let html = "";
  $(document).ready(function () {
    getBranch();
    getRequisition(); 
  });

  function getBranch(selectedName) {
    var dropDown = $("#selectBranch");
    var dropDown2 = $("#branch");

    $.ajax({
      type: "POST",
      dataType: "JSON",
      url: "./api/get_branch.php",
      data: {},
      success: function (response) {
        console.log("Success", response);
        var data = response.result;
        dropDown.append($("<option value='0'>ทุกสาขา</option>"));
        $.each(data, function (key, item) {
          dropDown.append(
            $("<option></option>")
              .attr(
                "selected",
                item.BranchNumber == selectedName ? "selected" : undefined
              )
              .attr("value", item.BranchNumber)
              .text(item.BranchName)
          );
          dropDown2.append(
            $("<option></option>")
              .attr(
                "selected",
                item.BranchNumber == selectedName ? "selected" : undefined
              )
              .attr("value", item.BranchNumber)
              .text(item.BranchName)
          );
        });
      },

      error: function (err) {
        console.log("Somthing wengworng pleass try again later", err);
      },
    });
  }

  $(document).on("click", ".btn_remove", function () {
    $(this).closest("tr").remove();
  });


  $('#exampleModal').keypress(function(e){
if (e.keyCode == 13)
{
  $('#txtProductName').focus()
}
});

  function handle(e, $item, $state) {
    if (e.keyCode === 13) {
      addProduct($item, $state);
    }
  }


  $("#rangestart").calendar({
    type: "date",
    initialDate: new Date(),
    formatter: {
      date: function (date, settings) {
        if (!date) return "";
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + "-" + month + "-" + day;
      },
    },
    endCalendar: $("#rangeend"),
  });
  $("#rangeend").calendar({
    type: "date",
    initialDate: new Date(),
    formatter: {
      date: function (date, settings) {
        if (!date) return "";
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return year + "-" + month + "-" + day;
      },
    },
    startCalendar: $("#rangestart"),
  });

  function date(date, settings) {
    if (!date) return "";
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    return year + "-" + month + "-" + day;
  }
</script>
