<?php
    session_start();
    if(!isset($_SESSION['username'])){
      $_SESSION['msg'] = "You must login first";
      header("location: login.php");
    }      
    if(isset($_GET['logout'])){
      session_destroy();
      unset($_SESSION['username']);
      header("login.php");
    }
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> Stock Control Systems</title>
  </head>
    <div class="container">
    <?php include('./view/header.php');?>
      <div id="dimmer" class="">
        <div class="ui text loader">Loading</div>
      </div>  
      <div class="row g-5">
        <div class="col-md-12">
          <!-- <h3 class="ui right aligned header"><a href="./logout.php">Log-out<i class="sign out alternate icon"></i></a></h3> -->
          <h4 class="mb-3"><h2 class="ui left aligned icon header">Stock Control System</h2>
          <h4 class="mb-3" style="padding: 10px;"></h4>
          <form class="needs-validation">
            <div class="card" style="padding: 10px;">
                
           
            <div class="row g-3">
           
              <div class="col-sm-3">
                <label class="form-label">Branch</label>
                <select
                  class="form-select"
                  name="selectBranch"
                  id="selectBranch"
                ></select>
              </div>

              <div class="col-sm-3">
                <label class="form-label">สถานะรายการ</label>
                <select
                  class="form-select"
                  name="overdue"
                  id="overdue"
                >
              <option value="0">ทุกรายการ</option>
              <option value="1">เฉพาะเกินกำหนด</option>
            </select>
              </div>
              
              <div class="col-sm-3">
                <label class="form-label">Product Group</label>
                <select class="form-select" id="selectGroup"></select>
              </div>

              <div class="col-sm-3">
                <label class="form-label">Brand</label>
                <select class="form-select" id="selectBrand"></select>
              </div>
              <div class="col-sm-4">
                <label class="form-label">Product Name</label>
                <input
                  type="text"
                  class="form-control"
                  id="txtProductName"
                  value=""
                />
              </div>

              <div class="col-sm-4">
                <label class="form-label">Barcode</label>
                <input
                  type="text"
                  class="form-control"
                  id="txtBarcode"
                  value=""
                />
              </div>

              <div class="col-sm-4">
                <label for="username" class="form-label">รหัสสินค้า</label>
                <div class="input-group has-validation">
                  <input type="text" class="form-control" id="txtPn" />
                </div>
              </div>
              

              <div class="col-md-3">
                <input
                id="btnSearch"
                  type="button"
                  class="btn btn-primary"
                  value="Search"
                  onclick="search_stock()"
                />
              </div>

              <hr class="my-4" />
            </div>
          </div>
            <div id="search_results" style="padding: 5px"></div>
          </form>
          <div class="card" style="padding: 0px;">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead class="table-light align-middle">
                  <tr>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col">Branch Name</th>

                    <th scope="col">ID</th>
                    <th scope="col">ProductCode</th>
                    <th scope="col">Barcode</th>
                    <th scope="col">Product Name</th>

                    <th scope="col">Number</th>
                    <th scope="col">Sunit</th>
                    <!-- <th scope="col">Price1</th>
                    <th scope="col">Price2</th>
                    <th scope="col">Price3</th>
                    <th scope="col">Price4</th>
                    <th scope="col">Price5</th>
                    <th scope="col">Min Price</th> -->
                    <th scope="col">จำนวน Serial</th>
                    <th scope="col">Product Group</th>
                    <th scope="col">ยี่ห้อสินค้า</th>
                    <th scope="col">ประกัน (วัน)</th>
                  </tr>
                </thead>
                <tbody id="tbodystock"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Create Modal-->
    <!-- Modal -->
    <div
      class="modal fade"
      id="exampleModal"
      tabindex="-1"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Stock Details</h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div class="modal-body">
            <div class="container-fluid">
              <div class="row">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Bill Type</th>
                        <th scope="col">ID</th>
                        <th scope="col">Bill Number</th>
                        <th scope="col">Number</th>
                        <!-- <th scope="col">ทุนก่อน Vat</th> -->
                        <th scope="col">SN</th>
                        <th scope="col">วันสินค้าเข้า Stock</th>
                      </tr>
                    </thead>
                    <tbody id="tbModal"></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>

    <!--Google Icon-->
    <link
      href="https://fonts.googleapis.com/css2?family=Material+Icons"
      rel="stylesheet"
    />
    <!--Bootstrap v5-->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
      crossorigin="anonymous"
    ></script>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <!--Jquery-->
    <script
      src="https://code.jquery.com/jquery-3.6.0.js"
      integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
      crossorigin="anonymous"
    ></script>
<!--Semitic UI-->
<script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
<script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>


<!--java script-->
    <script src="./js/index.js"></script>
  </body>
</html>
<script>

  $('body').keypress(function(e){
if (e.keyCode == 13)
{
    $("#btnSearch").focus();
    search_stock()
}
});
</script>
