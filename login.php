<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Login Stock - NextStep Reborn</title>
  </head>
  <style type="text/css">
    body {
      background-color: #585757;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <body>
    <div class="ui container"></div>
    <div class="ui middle aligned center aligned grid">
      
      <div class="column">
      <h2 class="ui teal image header">
  
  <div class="content">Stock Control System</div>
</h2>
      <div class="ui small images">
        <img src="./img/03-3b.jpg">
    <img src="./img/nextstep_logo_small.png">
  </div>
  

       
        <form class="ui large form">
          <div class="ui stacked segment">
            <div class="field">
              <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="text" id="username" placeholder="Username" />
              </div>
            </div>
            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" id="password" placeholder="Password" />
              </div>
            </div>
            <!-- <button type="button" class="ui fluid large teal submit button" id="login" onclick="login()">Login</button> -->
          </div>

        </form>
        <div class="ui hidden error message" id="err"></div>
        <button
          type="button"
          class="ui fluid large teal submit button"
          id="login"
          onclick="login()"
        >
          Login
        </button>
      </div>
    </div>

    <!--Bootstrap v5-->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
      crossorigin="anonymous"
    ></script>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <!--Semitic UI-->
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      crossorigin="anonymous"
    ></script>
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
  </body>
</html>

<script>
    $('body').keypress(function(e){
if (e.keyCode == 13)
{
  if($("#username").val() == ""){
    $("#username").focus();
  }else{
    $("#login").focus();
    login()
  }

}
});
  function login() {
    var email = $("#username").val();
    var password = $("#password").val();

    if (email != "" && password != "") {
      $.ajax({
        url: "./api/get_user.php",
        type: "POST",
        dataType: "JSON",
        data: {
          username: email,
          password: password,
        },
        cache: false,
        success: function (response) {
          console.log("Success", response);
          var data = response.result;

          if (data.statusCode == 200) {
            sessionStorage.setItem("roleUser", data.role);          
            location.href = "./index.php";
          } else if (data.statusCode == 201) {
            $("#err").removeClass().addClass("ui visible error message");
            $("#err").html("Invalid Username or Password !");
            
          }
        },
      });
    } else {
      $("#err").removeClass().addClass("ui visible error message");
      $("#err").html("Please check username or password field !");
    }
  }

  function validate() {
    var $valid = true;
    document.getElementById("user").innerHTML = "";
    document.getElementById("password").innerHTML = "";

    var userName = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    if (userName == "") {
      document.getElementById("user").innerHTML = "required";
      $valid = false;
    }
    if (password == "") {
      document.getElementById("password").innerHTML = "required";
      $valid = false;
    }
    return $valid;
  }
</script>
