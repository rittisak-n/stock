<?php


include_once('./dbconfig.php');


if($_SERVER['REQUEST_METHOD'] == "POST"){

      $query = "INSERT INTO Requisition (BranchNumber,UserReq,UserGive,RegTime,Status,Comment) VALUES (:branchNumber,:userReq,:userGive,CURRENT_TIMESTAMP,1,:comment)";
      $statement = $conn->prepare($query);
      
      $statement->execute(
       array(
        ':branchNumber' => $_POST["branchNumber"],
        ':userReq'  => $_POST["userReq"],
        ':userGive'   => '-',
        ':comment'   => $_POST['comment'],
       )
      );
      
      $statement = $conn->lastInsertId();
      $reqID = $conn->lastInsertId();
      echo json_encode($reqID);
      if(isset($reqID)){
        for($count = 0; $count<count($_POST["productCode"]); $count++){
        $sub_query = "INSERT INTO RequisitionDetail VALUES (:reqID, :productCode, :numStock, :numReq, :numGive, :reqRemark, :giveRemark)
        ";
        $statement = $conn->prepare($sub_query);
        $statement->execute(
         array(
          ':reqID' => $reqID,
          ':productCode'   => $_POST["productCode"][$count],
          ':numStock'    =>  number_format((int)$_POST["numStock"][$count]),
          ':numReq' =>  $_POST["numReq"][$count],
          ':numGive'    => 0,
          ':reqRemark'     => $_POST['reqRemark'][$count],
          ':giveRemark' => '-',
         )
        );
       }
      }
    
}
 
?>