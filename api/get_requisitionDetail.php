<?php


include_once('./dbconfig.php');

if($_SERVER['REQUEST_METHOD'] == "POST"){
	$reqid = isset($_POST['ReqID']) ? trim($_POST['ReqID']) : '';
	$sql="SELECT rd.*,UserReq,UserGive,RegTime,GiveTime,r.Comment,p.Name, b.BranchName, NumStock,RegTime FROM RequisitionDetail rd 
	LEFT JOIN Requisition r on rd.ReqID = r.ReqID 
	LEFT JOIN Product p on rd.ProductCode = p.ProductCode
	LEFT JOIN Branch b on r.BranchNumber = b.BranchNumber
	WHERE rd.ReqID = $reqid ";
	
		$q = $conn->query($sql);

		$data_arr = array();
		$data_arr['result'] = array();
		
		while($r = $q->fetch(PDO::FETCH_ASSOC)){
			$data_item = array(
				"ProductCode" => $r['ProductCode'],
				"NumReq" => $r['NumReq'],
				"NumGive" => $r['NumGive'],
				"UserGive" => $r['UserGive'],
				"GiveTime" => $r['GiveTime'],
				"UserReq" => $r['UserReq'],
				"GiveRemark" => $r['GiveRemark'],
				"ReqRemark" => $r['ReqRemark'],
				"Comment" => $r['Comment'],
				"ProductName" => $r['Name'],
				"BranchName" => $r['BranchName'],
				"NumStock" => $r['NumStock'],
				"RegTime" => $r['RegTime'],
				
			);
			array_push($data_arr['result'],$data_item);
		}

	echo json_encode($data_arr);
	http_response_code(200);
}else{
	http_response_code(405);
}
	

?>