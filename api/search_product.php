<?php
include_once('./dbconfig.php');

if($_SERVER['REQUEST_METHOD'] == "POST"){
    $state =  isset($_POST['state']) ? trim($_POST['state']) : '';
	if($state == "add" ){
		$sql="SELECT Distinct productCode,name,sum(Number) as NumStock  FROM Product p
		LEFT JOIN Stock s ON s.pn = p.pn  
		WHERE productcode IS NOT NULL AND  BranchNumber IN (2,5) AND (Name like '%" . $_POST["productname"] . "%'  OR productCode = '" . $_POST["productname"] . "')
		GROUP BY productCode,name";
	}
	else{
	$sql="SELECT Distinct productCode,name,sum(Number) as NumStock  FROM Product p
	LEFT JOIN Stock s ON s.pn = p.pn  
	WHERE productcode IS NOT NULL AND  BranchNumber IN (2,5) AND (Name like '%" . $_POST["productname"] . "%'  OR productCode like '%" . $_POST["productname"] . "%')
	GROUP BY productCode,name";
	}
	
	// $sql="SELECT productCode,name FROM Product ";
		$q = $conn->query($sql);

		$data_arr = array();
		$data_arr['result'] = array();
		
		while($r = $q->fetch(PDO::FETCH_ASSOC)){
			$data_item = array(
				"ProductCode" => trim($r['productCode']),
				"Name" => $r['name'],
				"NumStock" => number_format((int)$r['NumStock']),

			);
			array_push($data_arr['result'],$data_item);
		}

	echo json_encode($data_arr);
	http_response_code(200);
}else{
	http_response_code(405);
}
?>