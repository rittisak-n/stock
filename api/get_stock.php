<?php
include_once('./dbconfig.php');

// if($_SERVER['REQUEST_METHOD'] == "POST"){
	$branch =  isset($_POST['branch']) ? trim($_POST['branch']) : '';
	$barcode =  isset($_POST['barcode']) ? trim($_POST['barcode']) : '';
	$productName =  isset($_POST['productname']) ? trim($_POST['productname']) : '';
	$pn =  isset($_POST['pn']) ? trim($_POST['pn']) : '';
	$brand =  isset($_POST['brand']) ? trim($_POST['brand']) : '';
	$productgroup = isset($_POST['productgroup']) ? trim($_POST['productgroup']) : '';
	$overdue = isset($_POST['overdue']) ? trim($_POST['overdue']) : '';

	$sql = "SELECT Branch.BranchNumber,Branch.BranchName as BranchName ,stock.pn,ProductCode,Barcode,product.Name as ProductName,SUM(Number) as Number,product.SUnit,Price1,Price2,Price3,Price4,Price5,
	MinPrice,sum(stock.NumberS) as NumberS,ProductGroup.Name as ProductGroup,brand.Name as BrandName,AllowDecimal,Warranty,Point,
	CanUseTime = (SELECT TOP(1) CanUseTime FROM stock b WHERE b.pn = stock.pn ORDER By CanUseTime asc),CURRENT_TIMESTAMP as CurrentDate,
	(SELECT TOP(1) DATEDIFF(DAY, CanUseTime,CURRENT_TIMESTAMP) FROM stock b WHERE b.pn = stock.pn and b.BranchNumber = stock.BranchNumber ORDER By CanUseTime asc) as diffDate,NumAlert
		FROM stock
		LEFT JOIN product on product.PN = Stock.PN
		LEFT JOIN Branch on Branch.BranchNumber = Stock.BranchNumber
		LEFT JOIN ProductGroup on ProductGroup.ID = Product.GroupID
		LEFT JOIN brand on Brand.ID = product.BrandID
		LEFT JOIN ProductAlert on ProductAlert.pn = Stock.pn";
		// WHERE  stock.BranchNumber = $branch"; 
	if($branch != 0){
		$sql = $sql." WHERE  stock.BranchNumber = $branch"; 		
	}else{
		$sql = $sql." WHERE stock.BranchNumber <> $branch";
	}			
	if($barcode != null){
		$sql = $sql."and Barcode like '%$barcode%'";
	}
	if($productName!=null){
		$sql = $sql."and product.Name like '%$productName%'";
	}	
	if($pn != null){
		$sql = $sql."and stock.pn like '%$pn'";
	}
	if($brand!=null){
		$sql = $sql."and brand.id = '$brand'";
	}	
	if($productgroup!=null){
		$sql = $sql."and productgroup.id = '$productgroup'";
	}
	if($overdue == 1){
		$sql = $sql." and (SELECT TOP(1) DATEDIFF(DAY, CanUseTime,CURRENT_TIMESTAMP) FROM stock b WHERE b.pn = stock.pn and b.BranchNumber = stock.BranchNumber ORDER By CanUseTime asc) > NumAlert and NumAlert != 0 ";
	}				
	$sql = $sql."GROUP BY stock.pn,product.Name,stock.BranchNumber 	,Branch.BranchName	,Branch.BranchNumber,productcode,Barcode,product.SUnit,Price1,Price2,Price3,Price4,Price5,
	MinPrice,ProductGroup.Name,brand.Name,AllowDecimal,Warranty,Point,NumAlert
	ORDER BY Rtrim(Ltrim(product.Name)),ProductCode,Branch.BranchName";

$q = $conn->query($sql);
$results = array();

foreach($q as $r){
	$data_item = array(
		"BranchNumber" => $r['BranchNumber'],
		"BranchName" => $r['BranchName'],
		"Pn" => $r['pn'],
		"ProductCode" => trim($r['ProductCode']),
		"Barcode" => $r['Barcode'],
		"ProductName" => $r['ProductName'],
		"Number" => number_format((float)$r['Number'], 2, '.', ''),
		"SUnit" => trim($r['SUnit']),
		"Price1" => number_format((float)$r['Price1'], 2, '.', ','),
		"Price2" => number_format((float)$r['Price2'], 2, '.', ','),
		"Price3" => number_format((float)$r['Price3'], 2, '.', ','),
		"Price4" => number_format((float)$r['Price4'], 2, '.', ','),
		"Price5" => number_format((float)$r['Price5'], 2, '.', ','),
		"MinPrice" => number_format((float)$r['MinPrice'], 2, '.', ''),
		"NumberS" => number_format((float)$r['NumberS'], 2, '.', ''),
		"ProductGroup" => $r['ProductGroup'],
		"BrandName" => $r['BrandName'],
		"Warranty" => $r['Warranty'],
		"CanUseTime" => $r['CanUseTime'],
		"CurrentDate" => $r['CurrentDate'],
		"diffDate" => (int)$r['diffDate'],
		"Point" => number_format((float)$r['Point'], 2, '.', ''),
		"AllowDecimal" => $r['AllowDecimal'],
		"NumAlert" => (int)$r['NumAlert'],
	);
        $results[] = $data_item;  
}
echo json_encode($results);

?>