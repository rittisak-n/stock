<?php


include_once('./dbconfig.php');


if($_SERVER['REQUEST_METHOD'] == "POST"){
	$reqID =  isset($_POST['reqID']) ? trim($_POST['reqID']) : '';
	$userReq =  isset($_POST['userReq']) ? trim($_POST['userReq']) : '';
	$branch =  isset($_POST['branch']) ? trim($_POST['branch']) : '';
	$status = isset($_POST['status']) ? trim($_POST['status']) : '';
	$startDate = isset($_POST['startDate']) ? trim($_POST['startDate']) : '';
	$endDate = isset($_POST['endDate']) ? trim($_POST['endDate']) : '';

	$sql="SELECT Requisition.*,Branch.BranchName FROM requisition LEFT JOIN Branch on branch.BranchNumber = Requisition.BranchNumber";
	
// $sql="SELECT * FROM requisition ";

	if($branch == 0){
		$sql = $sql." WHERE requisition.BranchNumber <> $branch"; 		
	}else{
		$sql = $sql." WHERE requisition.BranchNumber = $branch";
	}

	if($status == 0){
		$sql = $sql." and requisition.status <> $status";		
	}else{
		$sql = $sql." AND requisition.status = $status";
	}

	if($reqID != ""){
		$sql = $sql." and reqID = $reqID "; 		
	}

	if($userReq != ""){
		$sql = $sql." and userReq like '%$userReq%' "; 		
	}

	if($startDate != "" && $endDate != ""){
		$sql = $sql." and RegTime between '$startDate 00:00:00' and '$endDate 23:59:59' "; 		
	}
	
		$q = $conn->query($sql);

		$data_arr = array();
		$data_arr['result'] = array();
		
		while($r = $q->fetch(PDO::FETCH_ASSOC)){
			$data_item = array(
				"ReqID" => $r['ReqID'],
				"BranchNumber" => $r['BranchNumber'],
				"BranchName" => $r['BranchName'],
				"UserReq" => $r['UserReq'],
                "UserGive" => $r['UserGive'],
                "RegTime"=> $r['RegTime'],
                "Status"=> $r['Status'],
                "Comment" => $r['Comment'],
				
			);
			array_push($data_arr['result'],$data_item);
		}

	echo json_encode($data_arr);
	http_response_code(200);
}else{
	http_response_code(405);
}
 
?>