<?php


include_once('./dbconfig.php');


if($_SERVER['REQUEST_METHOD'] == "POST"){

      $reqID = $_POST["reqID"];
      echo json_encode($reqID);
      if(isset($reqID)){
        for($count = 0; $count<count($_POST["productCode"]); $count++){
        $sub_query = "UPDATE RequisitionDetail
        SET NumGive = :numGive , GiveRemark = :giveRemark
        WHERE  ReqID = :reqID AND ProductCode = :productCode";
        $statement = $conn->prepare($sub_query);
        $statement->execute(
         array(
          ':reqID' => $reqID,
          ':productCode'   => $_POST["productCode"][$count],
          ':numGive'    => $_POST["giveNum"][$count],
          ':giveRemark' => $_POST["giveRemark"][$count]
         )
        );
       }

       $query = "UPDATE Requisition SET status = :status, GiveTime = CURRENT_TIMESTAMP, UserGive = :userGive WHERE ReqID = :reqID ";
       $statement = $conn->prepare($query);
       $statement->execute(
        array(
         ':reqID' => $reqID,    
         ':status'   => 2,
         ':userGive'   => $_POST["userGive"],
        )
       );
      }
    
}
 
?>