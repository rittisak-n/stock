<?php
include_once('./dbconfig.php');

// if($_SERVER['REQUEST_METHOD'] == "POST"){
	$branch =  isset($_POST['branch']) ? trim($_POST['branch']) : '';
	$pn =  isset($_POST['pn']) ? trim($_POST['pn']) : '';

	$sql = "SELECT stock.BranchNumber,stock.billType,stock.pn,stock.BillNumber,Number,Branch.BranchName,BuyPrice,stock.CanUseTime,
	DATEDIFF(DAY, stock.CanUseTime,CURRENT_TIMESTAMP) as diffDate, StockSerial.SN
		FROM stock
		LEFT JOIN Branch on Branch.BranchNumber = Stock.BranchNumber
		LEFT join StockSerial on StockSerial.PN = stock.PN 
		and stock.BranchNumber = StockSerial.BranchNumber and stock.BillNumber = StockSerial.BillNumber
		WHERE stock.pn = '$pn' and stock.BranchNumber=$branch"; 
$q = $conn->query($sql);
$results = array();

foreach($q as $r){
	$data_item = array(
        "billType" => $r['billType'],
		"BranchNumber" => $r['BranchNumber'],
		"BranchName" => $r['BranchName'],
        "BillNumber" => $r['BillNumber'],
		"Pn" => $r['pn'],
		"Number" => number_format((float)$r['Number'], 2, '.', ''),
		"BuyPrice" => number_format((float)$r['BuyPrice'], 2, '.', ','),
		"CanUseTime" => $r['CanUseTime'],
        "diffDate" => (int)$r['diffDate'],
		"SN" => $r['SN'],
	);
        $results[] = $data_item;  
}
echo json_encode($results);

?>