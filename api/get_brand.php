<?php
include_once('./dbconfig.php');

if($_SERVER['REQUEST_METHOD'] == "POST"){

	$sql="SELECT ID,Name FROM Brand Order by Name asc";
		$q = $conn->query($sql);

		$data_arr = array();
		$data_arr['result'] = array();
		
		while($r = $q->fetch(PDO::FETCH_ASSOC)){
			$data_item = array(
				"ID" => $r['ID'],
				"Name" => $r['Name'],

			);
			array_push($data_arr['result'],$data_item);
		}

	echo json_encode($data_arr);
	http_response_code(200);
}else{
	http_response_code(405);
}
?>