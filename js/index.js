var html, data;

$(document).ready(function () {
  render();
  getProductGroup();
  getProductBrand();
});

function render(selectedName) {
  var dropDown = $("#selectBranch");
  dropDown.empty();

  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/get_branch.php",
    data: {},
    success: function (response) {
      console.log("Success", response);
      var data = response.result;
      dropDown.append($("<option value='0'>ทุกสาขา</option>"));
      $.each(data, function (key, item) {
        dropDown.append(
          $("<option></option>")
            .attr(
              "selected",
              item.BranchNumber == selectedName ? "selected" : undefined
            )
            .attr("value", item.BranchNumber)
            .text(item.BranchName)
        );
      });
    },

    error: function (err) {
      console.log("Somthing wengworng pleass try again later", err);
    },
  });
}

function getProductGroup(selectedName) {
  var dropDown = $("#selectGroup");
  dropDown.empty();

  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/get_productGroup.php",
    data: {},
    success: function (response) {
      console.log("Success", response);
      var data = response.result;
      dropDown.append($("<option value=''>ไม่ระบุกลุ่มสินค้า</option>"));
      $.each(data, function (key, item) {
        dropDown.append(
          $("<option></option>")
            .attr("selected", item.ID == selectedName ? "selected" : undefined)
            .attr("value", item.ID)
            .text(item.Name)
        );
      });
    },

    error: function (err) {
      console.log("Somthing wengworng pleass try again later", err);
    },
  });
}

function getProductBrand(selectedName) {
  var dropDown = $("#selectBrand");
  dropDown.empty();

  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/get_brand.php",
    data: {},
    success: function (response) {
      console.log("Success", response);
      var data = response.result;
      dropDown.append($("<option value=''>ไม่ระบุยี่ห้อสินค้า</option>"));
      $.each(data, function (key, item) {
        dropDown.append(
          $("<option></option>")
            .attr("selected", item.ID == selectedName ? "selected" : undefined)
            .attr("value", item.ID)
            .text(item.Name)
        );
      });
    },

    error: function (err) {
      console.log("Somthing wengworng pleass try again later", err);
    },
  });
}

function search_stock() {
  var table = $("#tbodystock"); 
  var branch = $("#selectBranch").val();
  var barcode = $("#txtBarcode").val();
  $("#dimmer").toggleClass("ui active dimmer");
  // var productname = $("#txtProductName").val();
  $.ajax({
    url: "./api/get_stock.php",
    type: "POST",
    dataType: "JSON",
    data: {
      branch: branch,
      barcode: barcode,
      productname: $("#txtProductName").val(),
      pn: $("#txtPn").val(),
      productgroup: $("#selectGroup").val(),
      brand: $("#selectBrand").val(),
      overdue: $("#overdue").val(),
    },
    success: function (returnData) {
      $("#dimmer").toggleClass("ui active dimmer");
      var tableDanger;
      table.html("");
      var results = returnData;
      let i = 1;
      console.log(results)
      $("#search_results").html(
        "พบรายการทั้งหมด " + results.length + " รายการ"
      );
      if (results.length == 0) {
        table.append('<tr><td colspan="12"> No results found! </td></tr>');
      } else {
        $.each(results, function (key, item) {
          if (item.Warranty >= 365) {
            Warranty = item.Warranty / 365 + " ปี";
          } else if (item.Warranty >= 30) {
            Warranty = item.Warranty / 30 + " เดือน";
          } else {
            Warranty = item.Warranty;
          }
          let classTablel
          if (item.NumAlert != 0 && (item.diffDate > item.NumAlert)) {
            // tableDanger = "table-danger";
            tableDanger = "style='color:crimson ; background: rgb(218, 13, 13); color: rgb(245, 241, 241)'"

          } else {
            tableDanger = "";
          }
          table.append(`<tr class="" ${tableDanger}>
          <th scope="row">${i++}</th>
            <th scope="row">              
            <button type="button" class="btn btn-default" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="search_stockById(${
              item.BranchNumber
            },${item.Pn},${item.NumAlert})">
            <span class="material-icons">search</span>
          </button></th>
            <th class="col-md-2">${item.BranchName}</th>
            <td>${item.Pn}</td>
            <td>${item.ProductCode}</td>
            <td>${item.Barcode}</td>
            <td>${item.ProductName}</td>
            <td>${item.Number}</td>
            <td>${item.SUnit}</td>
           <!-- <td>${item.Price1}</td>
            <td>${item.Price2}</td>
            <td>${item.Price3}</td>
            <td>${item.Price4}</td>
            <td>${item.Price5}</td>
            <td>${item.MinPrice}</td> -->
            <td>${item.NumberS}</td>
            <td>${item.ProductGroup}</td>
            <td>${item.BrandName}</td>
            <td>${Warranty}</td>
            </tr>
            `);
        });
      }
    },
  });
}

function search_stockById($branch, $pn, $numalert) {
  let table = $("#tbModal");
  let branch = $branch;
  let pn = $pn;
  let billtype;
  
  
  $.ajax({
    url: "./api/get_stockById.php",
    type: "POST",
    dataType: "JSON",
    data: {
      branch: branch,
      pn: pn,
    },
    success: function (returnData) {
      let tableDanger;
      let numalert = $numalert;
      table.html("");
      var results = returnData;
      if (results.length == 0) {
        table.append('<tr><td colspan="12"> No results found! </td></tr>');
      } else {
        $.each(results, function (key, item) {
          if (item.billType == 0) {
            billtype = "ซื้อ";
          } else if (item.billType == 2) {
            billtype = "โอนต่างสาขา";
          }
         
           
            if (numalert != 0 && (item.diffDate > numalert)) {
              tableDanger = "table-danger";
            } else {
              tableDanger = "";
            }
          
 

          table.append(`<tr class="${tableDanger}">
          <td>${billtype}</td>
            <td>${item.Pn}</td>
            <td>${item.BillNumber}</td>
            <td>${item.Number}</td>
           <!-- <td>${item.BuyPrice}</td>-->
            <td>${item.SN}</td>
            <td>${item.CanUseTime}</td>
            </tr>
            `);
        });
      }
    },
  });
}
