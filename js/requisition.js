let array = [];
let productId = [];
let unit = [];
let reqRemark = [];
let giveNum = [];
let giveRemark = [];
let reqID;
let numStock = [];

function search_product($productname) {
  var content = [];
  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/search_product.php",
    data: "productname=" + $productname,
    success: function (response) {
      console.log("Success", response);
      var data = response.result;
      $.each(data, function (key, item) {
        content.push({ title: item.Name, description: item.ProductCode });
      });
      //fill search form
      console.log(content.description)
      $(".ui.search").search({
        source: content,
        searchFields: ["title"]["description"],
        searchFullText: false,
        onSelect: function(content){
          addProduct(content.description, 1)
      },
      });
    },
    error: function (err) {
      console.log("Somthing wengworng pleass try again later", err);
    },
  });
}

html = "";
function getRequisition($state) {
  var branch = $("#selectBranch").val() == null ? 0 : $("#selectBranch").val();
  let role = sessionStorage.getItem("roleUser");
  let startDate = $("#startDate").val();
  let endDate = $("#endDate").val();
  let status = $("#status").val();
  if ($state == 1) {
    status = 1;
    startDate = "";
    endDate = "";
  }

  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/get_requisition.php",
    data: {
      branch: branch,
      reqID: $("#searchNumber").val(),
      userReq: $("#searchName").val(),
      status: status,
      startDate: startDate,
      endDate: endDate,
    },
    success: function (response) {
      console.log("Success", response);
      var data = response.result;
      let i = 1;
      let colorStatus = "";
      let iconStatus = "";
      let roleuser = role;
      html = "";

      if ($state == 1) {
        $("#tbodyWaitApprove").html("");
      } else {
        $("#tbody").html("");
      }
      if (data.length == 0) {
        $("#tbody").append(
          '<tr><td colspan="12"> No results found! </td></tr>'
        );
      } else {
        $.each(data, function (key, item) {
          if (item.Status == 1) {
            iconStatus = "รอดำเนินการ";
            colorStatus = "ui red table";
          } else if (item.Status == 2) {
            iconStatus = "ดำเนินการเรียบร้อย";
            colorStatus = "";
          }
          let newdate = moment(item.RegTime).format("DD-MM-YYYY - HH:mm A");

          html += `<tr class="center aligned ${colorStatus}">
            <td class="collapsing">${i++}</td>
            <td class="collapsing">${item.ReqID}</td>
            <td class="">${item.BranchName}</td>
            <td class="">${item.UserReq}</td>
            <td class="collapsing">${iconStatus}</td>
            <td class="">${newdate}</td>`;
          if ($state == 1) {
          } else {
            html += ` <td class="collapsing">           
           <div class="ui small basic icon buttons">
              <button class="ui button" data-bs-toggle="modal" data-bs-target="#modalDetail" onclick="requisitionDetail(${
                item.ReqID
              },${1})"><i class="search icon"></i></button>`;
            if (roleuser == 1) {
              html += `
            <button class="ui ${
              item.Status == 2 ? "disabled" : ""
            } button" data-bs-toggle="modal" data-bs-target="#modalEdit" onclick="requisitionDetail(${
                item.ReqID
              },${2})"><i class="edit icon"></i></button>
      
      <button class="ui button" data-bs-toggle="modal" data-bs-target="#modalCancel"  data-userid="${
        item.ReqID
      }"><i class="cancel icon"></i></button>`;
            }
            html += `
            <button class="ui button" onclick="printPage(${item.ReqID})"><i class="print icon"></i></button> 
          </div>
            </td>
            </tr>
            `;
          }
          if ($state == 1) {
            $("#tbodyWaitApprove").html(html);
          } else {
            $("#tbody").html(html);
          }
        });
      }
    },
    error: function (err) {
      console.log("Somthing wengworng pleass try again later", err);
    },
  });
}

function getDataTable($state) {
  if ($state == 1) {
    if ($(".productCode").length) {
      $(".productCode").each(function () {
        productId.push($(this).data("pdp-id"));
      });
      $(".numReq").each(function () {
        unit.push($(this).val());
      });
      $(".reqRemark").each(function () {
        reqRemark.push($(this).val());
      });
      $(".numStock").each(function () {
        numStock.push(parseInt($(this).data("stock-num").toString().replace(/\,/g,'')))
      });
    }
  } else {
    $(".editProduct").each(function () {
      productId.push($(this).data("pdp-id"));
    });
    $(".giveNum").each(function () {
      giveNum.push($(this).val());
    });
    $(".giveRemark").each(function () {
      giveRemark.push($(this).val());
    });
  }
}

function addProduct($item, $state) {
  let productname = $item;
  productname.trim();

  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/search_product.php",
    data: {
      productname: productname,
      state: 'add',
    },
    success: function (response) {
      console.log("Success", response);
      var data = response.result;
      let i = 0;
      productId = [];
      getDataTable(1);

      if (data.length == 1) {
        $.each(data, function (key, item) {
          if (productId.includes(item.ProductCode)) {
            alert("มีรายการที่เลือกแล้ว");
          } else {
            productId.push(item.ProductCode);
            $("#dynamic_field").append(`
          <tr class="productCode"  data-pdp-id="${item.ProductCode}">
          <td>${item.ProductCode}</td>
          <td>${item.Name}</td>
          <td class="numStock" data-stock-num="${item.NumStock}">${item.NumStock}</td>
          <td class=""><input type="number" class="numReq" value="1" min="1" /></td>
          <td><textarea class="reqRemark" id="reqRemark" rows="3"></textarea></td>
          <td><button type="button" id="${item.ProductCode}" class="ui icon button btn_remove"><i class="close icon"></i></button></td>
          </tr>
         `);
          }
          $(".prompt").val("");
        });
      } else {
        alert("ไม่พบรายการที่ค้นหา");
      }
    },
    error: function (err) {
      console.log("Somthing wengworng pleass try again later", err);
    },
  });
}

function createReq() {
  if ($("#userReq").val() == "" || $("#userReq").val() == undefined) {
    alert("Please enter requisition name !!");
  } else if (productId.length == 0) {
    alert("Please enter product !!");
  } else {
    productId = [];
    unit = [];
    reqRemark = [];
    numStock = [];
    getDataTable(1);
   
    $.ajax({
      type: "POST",
      dataType: "JSON",
      url: "./api/create_requisition.php",
      data: {
        userReq: $("#userReq").val(),
        productCode: productId,
        numReq: unit,
        branchNumber: $("#branch").val(),
        reqRemark: reqRemark,
        numStock: numStock,
        comment: $("#comment").val(),
      },
      success: function (response) {
        console.log("Create Success", response);
        $("#exampleModal").modal("toggle");
        $("#dynamic_field").html("");
        $("#userReq").val("");
        $("#modalSuccess").modal("show");
        $("#comment").val("");
        productId = [];
        unit = [];
        reqRemark = [];
        numStock = [];
        getRequisition();
      },
      error: function (err) {
        console.log(err.responseText);
      },
    });
  }
}

function requisitionDetail($reqID, $state) {
  reqID = $reqID;
  let state = $state;
  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/get_requisitionDetail.php",
    data: {
      ReqID: reqID,
    },
    success: function (response) {
      console.log("Success", response);
      var data = response.result;
      console.log(data);
      let newdate =
        data[0].GiveTime == null || data[0].GiveTime == undefined
          ? ""
          : moment(data[0].GiveTime).format("DD-MM-YYYY - HH:mm A");

      $("#requisitionDetails").html("");
      $(".headerLeft").html(`
      <h4 class="ui header">เลขที่ใบเบิก: ${reqID}</h4>
      <h4 class="ui header">ชื่อผู้ขอเบิก: ${data[0].UserReq}</h4>
      <h4 class="ui header">สาขา: ${data[0].BranchName}</h4>
      <h4 class="ui header">หมายเหตุ: ${data[0].Comment}</h4>`);

      $(".headerRight").html(`
      <h4 class="ui header"></h4>
      <h4 class="ui header">ชื่อผู้อนุมัติ: ${data[0].UserGive}</h4>
      <h4 class="ui header">วันที่อนุมัติ: ${
        data[0].GiveTime == null ? "" : newdate
      }</h4>`);
      if (state == 1) {
        $.each(data, function (key, item) {
          $("#requisitionDetails").append(`
          <tr>
          <td>${item.ProductCode}</td>
          <td>${item.ProductName}</td>
          <td>${item.NumStock}</td>
          <td>${item.NumReq}</td>
          <td>${item.NumGive}</td>
          <td>${item.ReqRemark}</td>
          <td>${item.GiveRemark}</td>  
          </tr>
       `);
        });
      } else if (state == 2) {
        $("#editRequisitionDetails").html("");

        $.each(data, function (key, item) {
          $("#editRequisitionDetails").append(`
          <tr class="editProduct" data-pdp-id="${item.ProductCode}">
          <td class="">${item.ProductCode}</td>
          <td class="">${item.ProductName}</td>
          <td class="">${item.NumStock}</td>
          <td class="">${item.NumReq}</td>
          <td class=""><input type="number" class="giveNum" min="0"  value="0"/></td>
          <td class=""><input type="text" class="giveRemark" value="${item.GiveRemark}"/></td>
          </tr>
       `);
        });
      }
    },
    error: function (err) {
      console.log(err.responseText);
    },
  });
}

function updateRequisitionDetail() {
  productId = [];
  giveNum = [];
  giveRemark = [];
  getDataTable(2);
  if ($("#userGive").val() == "" || $("#userGive").val() == undefined) {
    alert("Please enter name giver !!");
    return;
  }

  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/update_requisition.php",
    data: {
      reqID: reqID,
      productCode: productId,
      giveNum: giveNum,
      giveRemark: giveRemark,
      userGive: $("#userGive").val(),
    },
    success: function (response) {
      console.log("Update Success", response);
      $("#modalEdit").modal("toggle");
      $(".editProduct").html("");
      $("#userGive").val("");
      $("#modalSuccess").modal("show");
      productId = [];
      unit = [];
      giveRemark = [];
      getRequisition();
    },
    error: function (err) {
      console.log(err.responseText);
    },
  });
}

let cancelModalId;
$("#modalCancel").on("show.bs.modal", function (e) {
  cancelModalId = $(e.relatedTarget).data("userid");
});
function cancelRequisition() {
  let cancelReqID;

  cancelReqID = cancelModalId;

  $.ajax({
    type: "POST",
    dataType: "JSON",
    url: "./api/delete_requisition.php",
    data: {
      reqID: cancelReqID,
    },
    success: function (response) {
      console.log("Cancel Success", response);
      $("#modalCancel").modal("toggle");
      getRequisition();
    },
    error: function (err) {
      console.log(err.responseText);
    },
  });
}

function printPage($ReqID) {
  let reqID = $ReqID;
  localStorage.setItem("id", reqID);
  // window.open("./pdf.php");
  window.open("./pdf.php");
}
