<style type="text/css" media="print">
  @media print {
    @page {
      margin: 30;
    }
    body {
      margin: 1.6cm;
    }
  }
</style>
<div class="ui container">

  <div class="ui grid">
    <div class="four wide column">
      <img class="ui Medium image" src="./img/nextstep_logo_small.png" />
    </div>
    <div class="four wide column"></div>
    <div class="three wide column"></div>
    <div class="five wide column" id="date"></div>
  </div>
  <!-- <div class="ui divider"></div> -->
  <div class="ui grid">
    <div class="eight wide column">
      <h2 class="ui header">
        Requisition By
        <div class="sub header" id="reqUser">
        </h2>
      </div>
      <div class="eight wide column">
        <h2 class="ui header">
          Approve By
          <div class="sub header" id="approveDate">
          </h2>
        </div>
      </div>
      
      <div class="ui divider"></div>
      <div class="ui grid">
        <div class="sixteen wide column">
          <h3 class="ui header">Requisition Summary</h3>
          <table class="ui table">
            <thead>
              <tr>
                <th class="collapsing center aligned">Product Code</th>
                <th class="collapsing center aligned">Product Name</th>
                <th class="collapsing center aligned">จำนวนที่ขอ</th>
                <th class="collapsing center aligned">จำนวนที่ให้</th>
                <th class="collapsing center aligned">หมายเหตุ ผู้ขอ</th>
                <th class="collapsing center aligned">หมายเหตุ ผู้ให้</th>
              </tr>
            </thead>
            <tbody id="tbodyDetail"></tbody>
          </table>
        </div>
      </div>
      <!-- <div class="ui inverted segment">
        <p>Please contact your system admin if you have problems.</p>
      </div> -->
    </div>
  </div>
</div>



<!--Semitic UI-->
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  crossorigin="anonymous"></script>
<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"
  />
<script
  src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="./js/requisition.js"></script>

<script>
  $(document).ready(function () {
    getDataPrint();
  });

  function getDataPrint() {
    var ReqID = localStorage.getItem("id");
    // localStorage.clear(); //clean the localstorage
    console.log(ReqID);
    $.ajax({
      type: "POST",
      dataType: "JSON",
      url: "./api/get_requisitionDetail.php",
      data: {
        ReqID: ReqID,
      },
      success: function (response) {
        console.log("Success", response);
        var data = response.result;
        let status = data[0].status == null ? "Wait Approved" : "Approved"
        let createDate = moment(data[0].RegTime).format("MMM D, YYYY - HH:mm A");
        let approveDate =
          data[0].GiveTime == null
            ? "-"
            : moment(data[0].GiveTime).format("MMM D, YYYY - HH:mm A");
        $("#date").html("<b>Date: &nbsp; </b> " + moment().format("YYYY MM DD - HH:mm A"));
        // $("#reqUser").append("<div class='sub header'>"+
        //   "<b>Name:&nbsp; &nbsp;</b> " +
        //     data[0].UserReq +
        //     "<br><b>Branch:&nbsp; &nbsp;</b>" +
        //     data[0].BranchName +
        //     "<br><strong>Requisition Date: &nbsp; &nbsp; </strong>"+createDate+
        //     "<br><br><b>หมายเหตุ: </b> " +
        //     data[0].Comment + "</div>"
        // );
        // $("#reqDate").append("<strong>Requisition Date: &nbsp; &nbsp; </strong>"+createDate + "");
        // $("#userApprove").append("<div class='sub header'>Name: " + data[0].UserGive + "<br>" + "</div>"+"<strong>Approve Date: &nbsp; &nbsp; </strong>"+approveDate);
        $("#approveDate").append("Name: &nbsp; &nbsp;" +  data[0].UserGive + "<br>Status: &nbsp; &nbsp;"+ status +"<br><br>Approved  Date: &nbsp; &nbsp; "+approveDate);
        $("#reqUser").append("Name: &nbsp; &nbsp;" +  data[0].UserReq + "<br>Requisition Number: &nbsp; &nbsp"+ ReqID +"<br>Branch:&nbsp; &nbsp;"+data[0].BranchName+"<br>Requisition Date: &nbsp; &nbsp; "+ createDate  + "<br><br>Note: &nbsp; &nbsp; " +data[0].Comment );

        $.each(data, function (key, item) {
          $("#tbodyDetail").append(`
          <tr class="center aligned">
          <td>${item.ProductCode}</td>
          <td>${item.ProductName}</td>
          <td>${item.NumReq}</td>
		  <td>${item.NumGive}</td>
		  <td>${item.ReqRemark}</td>
		  <td>${item.GiveRemark}</td>
          </tr>
       `);

       
        });

        setTimeout(function () {
          //your code here
          printer();
        }, 10);
        // localStorage.clear(); //clean the localstorage
      },
      error: function (err) {
        console.log(err.responseText);
      },
    });
  }
  function printer() {
    window.print();
    window.close();
  }
</script>
